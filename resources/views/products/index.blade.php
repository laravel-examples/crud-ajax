@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <h2>Laravel 7 - {{ $title }}</h2>
            </div>
            <div class="float-right">
                <button class="btn btn-success" type="button" id="createNewProduct">
                    Create New Product
                </button>
            </div>
        </div>
    </div>

    <table class="table table-bordered" id="productsDatatable" width="100%">
        <thead>
            <tr>
                <th>N°</th>
                <th>Name</th>
                <th>Details</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- The Modal -->
    <div class="modal fade" id="productModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="productModalTitle"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <form action="" method="post" id="productForm">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="name"><strong>Name</strong></label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Enter product name">
                    </div>
                    <div class="form-group">
                        <label for="detail"><strong>Details</strong></label>
                        <textarea class="form-control" name="detail" id="detail" placeholder="Enter product details"></textarea>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit" value="Submit">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var products_datatable = $('#productsDatatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('products.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'detail', name: 'detail'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#createNewProduct').click(function(event) {
            $('#productModalTitle').text('Create Product');
            $('#productModal').modal('show');
        });

        $('#productsDatatable tbody').on('click', '.editProduct', function(event) {
            var product_id = $(this).data('id');

            $.ajax({
                url: '{{ route("products.edit", ":id") }}'.replace(':id', product_id),
                type: "GET",
                data: $('#productForm').serialize(),
                datatype: 'json',
                success: function(data) {
                    $('#productForm').trigger('reset');
                    $('#productModalTitle').text('Edit Product');
                    $('#id').val(data.id);
                    $('#name').val(data.name);
                    $('#detail').val(data.detail);
                    $('#productModal').modal('show');
                },
                error: function(data) {
                    alert('Error: ' + JSON.stringify(data));
                    products_datatable.draw();
                }
            });
        });

        $('#productForm').submit(function(event) {
            event.preventDefault();

            $.ajax({
                url: "{{ route('products.store') }}",
                type: "POST",
                data: $('#productForm').serialize(),
                datatype: 'json',
                success: function(data) {
                    $('#productModal').modal('hide');
                    $('#productForm').trigger('reset');
                    $('#id').val("");
                    products_datatable.draw();
                },
                error: function(data) {
                    alert('Error: ' + JSON.stringify(data));
                    products_datatable.draw();
                }
            });
        });

        $('#productsDatatable tbody').on('click', '.deleteProduct', function(event) {
            var product_id = $(this).data('id');

            var delete_product = confirm('Are you sure want to delete?');

            if (delete_product == true) {
                $.ajax({
                    url: '{{ route("products.destroy", ":id") }}'.replace(':id', product_id),
                    type: "DELETE",
                    datatype: 'json',
                    success: function(data) {
                        products_datatable.draw();
                    },
                    error: function(data) {
                        alert('Error: ' + JSON.stringify(data));
                        products_datatable.draw();
                    }
                });
            } else {
                products_datatable.draw();
            }
        });
    });
</script>
@endsection
